﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateObjects : MonoBehaviour
{
    public GameObject obstacle;
    float timer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        manageTimer(); 
    }

    void manageTimer()
    {
        timer += Time.deltaTime;

        if (timer >= 2)
        {
            addObstacle();
            timer = 0;
        }
    }

    void addObstacle()
    {
        int randomNumber = Random.Range(1, 4); // return a random number between 1 and 3. Random.Range max value is exclusive.

        Vector3 positionOfPlayer = GameObject.Find("Player").GetComponent<PlayerMovement>().initialPosition;
        GameObject t1;
        t1 = (GameObject)(GameObject.Instantiate(obstacle, positionOfPlayer + Vector3.right * 20, Quaternion.identity));
    }
}
