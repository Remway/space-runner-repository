﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateTiles : MonoBehaviour
{
    [SerializeField] private Transform Tile_Start;
    [SerializeField] private Transform Tile_Outdoor_Short;
    [SerializeField] private Transform Tile_Outdoor_Medium;
    [SerializeField] private Transform Tile_Outdoor_Long;
    [SerializeField] private Transform Tile_Indoor_Short;
    [SerializeField] private Transform Tile_Indoor_Medium;
    [SerializeField] private Transform Tile_Indoor_Long;
    [SerializeField] private Transform Obstacle_Worm;
    [SerializeField] private Transform Obstacle_Alien;
    [SerializeField] private Transform Obstacle_Flying;

    private Vector3 lastEndPosition;
    private Transform lastObstaclePosition;
    private int tileCounter = 0;
    private Transform lastTileTransform;
    private const float PLAYER_DISTANCE_FROM_END_TRIGGER = 10.0f;

    private void Awake()
    {
        lastEndPosition = Tile_Start.Find("EndPosition").position;
        lastObstaclePosition = Tile_Start.Find("EndPosition");
        for (int i = 0; i < 10; i++)
        {
            spawnTile();
            spawnObstacle(lastObstaclePosition);
        }
    }

    public void spawnTile()
    {
        lastTileTransform = spawnTile(lastEndPosition);
        lastEndPosition = lastTileTransform.Find("EndPosition").position;
        lastObstaclePosition = lastTileTransform.Find("ObstacleSpawn");
    }

    private void Update()
    {
        lastEndPosition = lastTileTransform.Find("EndPosition").position;
        if (Vector3.Distance(GameObject.FindWithTag("Player").transform.position, lastEndPosition) < PLAYER_DISTANCE_FROM_END_TRIGGER)
        {
            spawnTile();
            spawnObstacle(lastObstaclePosition);
        }
    }

  private Transform spawnObstacle(Transform spawnPosition)
    {
        Transform obstaclePosition = lastObstaclePosition;
        int randomNumber = Random.Range(1, 4); // return a random number between 1 and 3. Random.Range max value is exclusive.
        if (tileCounter <= 20)
        { // outdoor tiles
            if (randomNumber == 1)
                obstaclePosition = Instantiate(Obstacle_Alien, spawnPosition.position, Quaternion.identity);
            else if (randomNumber == 2)
                obstaclePosition = Instantiate(Obstacle_Flying, spawnPosition.position, Quaternion.identity);
            else if (randomNumber == 3)
                obstaclePosition = Instantiate(Obstacle_Alien, spawnPosition.position, Quaternion.identity); // 2/3 chance for walking alien.
        }
        else
        {   //indoor tiles
            if (randomNumber == 1)
                obstaclePosition = Instantiate(Obstacle_Worm, spawnPosition.position, Quaternion.identity);
            else if (randomNumber == 2)
                obstaclePosition = Instantiate(Obstacle_Worm, spawnPosition.position, Quaternion.identity);
            else if (randomNumber == 3)
                obstaclePosition = Instantiate(Obstacle_Worm, spawnPosition.position, Quaternion.identity); //only using worms and hanging obstacles (which spawn on large tiles only guareenteed)
        }
        return obstaclePosition;
    }

    private Transform spawnTile(Vector3 spawnPosition)
    {
        int randomNumber = Random.Range(1, 4); // return a random number between 1 and 3. Random.Range max value is exclusive.
        Transform tileTransform = lastTileTransform;

        if (tileCounter <= 20)
        { // outdoor tiles
            if (randomNumber == 1)
            {
                tileTransform = Instantiate(Tile_Outdoor_Short, spawnPosition, Quaternion.identity);
            }
            //spawn indoor obstacle.
            else if (randomNumber == 2)
            {
                tileTransform = Instantiate(Tile_Outdoor_Medium, spawnPosition, Quaternion.identity);
            }
            else if (randomNumber == 3)
            {
                tileTransform = Instantiate(Tile_Outdoor_Long, spawnPosition, Quaternion.identity);
            }
        }
        else
        {   //indoor tiles
            if (randomNumber == 1)

                tileTransform = Instantiate(Tile_Indoor_Short, spawnPosition, Quaternion.identity);
            else if (randomNumber == 2)

                tileTransform = Instantiate(Tile_Indoor_Medium, spawnPosition, Quaternion.identity);
            else if (randomNumber == 3)

                tileTransform = Instantiate(Tile_Indoor_Long, spawnPosition, Quaternion.identity);

            //reset
            if (tileCounter == 40)
                tileCounter = 0;
        }
        tileCounter++; // increment tile counter
        return tileTransform;
    }
}


/* TO DO
 *
 * Once we have all our obstacles done, create obstacle pool and spawn them randomly on appropriate tiles.
 *
 * make tiles move and destroy
 */