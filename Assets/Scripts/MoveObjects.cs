﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObjects : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // move objects left at this speed.
        transform.Translate(Vector2.left * 4 * Time.deltaTime); // will need to add difficulty variant.   

        // will delete objects when they reach position.x = -10.
        if (transform.position.x < -10)
            Destroy(gameObject);
    }
}
