﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D rb2d;
    public float jumpSpeed;
    public Vector3 initialPosition;
    
    // will need if statement for gravity and to set depending on indoor outdoor SA

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        initialPosition = transform.position;
    }
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal"); // for testing collision boxes without autorun. will need to remove this line later. SA
        float moveVertical = Input.GetAxis("Vertical");
        Vector2 movement = new Vector2( moveHorizontal, moveVertical); // will need to take moveHorizontal and replace with a speed variable or 0. SA
        movement *= Time.fixedDeltaTime;
        rb2d.AddForce(movement * jumpSpeed);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb2d.velocity = Vector2.up * jumpSpeed * Time.deltaTime;
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        Debug.Log("Game Over");
    }
}
